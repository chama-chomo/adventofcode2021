use flexi_logger::Logger;
use log::{debug, info, warn};
use std::env;
use std::error::Error;
use std::fs;

fn main() {
    let mut days: usize = 80;

    Logger::try_with_str("debug").unwrap().start().unwrap();

    let cl_args: Vec<String> = env::args().collect();

    // obtain input data
    let mut task_data: Vec<u8> = read_file(&cl_args[2])
        .unwrap()
        .split(",")
        .map(|s| s.trim())
        .map(|s| s.parse::<u8>().unwrap())
        .collect::<Vec<_>>();

    info!("Input: {:?}", task_data);

    while days != 0 {
        days -= 1;
        for (index, n) in task_data.clone().iter().enumerate() {
            if n == &0 {
                task_data[index] = 6;
                task_data.push(8)
            } else {
                task_data[index] = n - 1
            }
        }
    }

    info!("Lanternfish found: {}", task_data.len())
}

fn read_file(file_path: &str) -> Result<String, Box<dyn Error>> {
    info!("Reading the input file.");
    let contents = fs::read_to_string(file_path)?;
    debug!("File contents: {}", &contents);
    info!("End of Reading the input file.");

    Ok(contents)
}
