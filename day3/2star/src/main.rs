use flexi_logger::Logger;
use log::{debug, info};
use std::env;
use std::error::Error;
use std::fs;

fn main() {
    Logger::try_with_str("debug").unwrap().start().unwrap();

    let cl_args: Vec<String> = env::args().collect();

    let task_data: Vec<String> = read_file(&cl_args[2])
        .unwrap()
        .lines()
        .map(|line| line.trim().to_string())
        .collect();

    debug!("Accepted input data.. len={}", &task_data.len());

    let row_len: usize = task_data.len().to_owned() + 1;

    let ogr = task_data.clone();
    let sr = task_data.clone();

    let ogr: isize = find_uniq_number(ogr, "1", "0", row_len);
    let sr: isize = find_uniq_number(sr, "0", "1", row_len);

    info!("Life support rating: {}", ogr * sr)
}

fn find_uniq_number(
    mut task_data: Vec<String>,
    most_common: &str,
    least_common: &str,
    row_len: usize,
) -> isize {
    for index in 0..row_len {
        let mut column: Vec<u32> = vec![];
        for row in &task_data {
            let transformed_data: Vec<u32> =
                row.chars().map(|char| char.to_digit(10).unwrap()).collect();
            column.push(transformed_data[index]);
        }

        let ones = column.iter().filter(|&n| *n == 1).count();
        let zeros = column.iter().filter(|&n| *n == 0).count();

        println!("1:{} 0:{}", ones, zeros);

        if ones + zeros < 3 {
            break;
        }

        if ones >= zeros {
            task_data.retain(|elem| elem[index..index + 1].to_string() == most_common);
        } else {
            task_data.retain(|elem| elem[index..index + 1].to_string() == least_common);
        }
    }
    binary_to_10(&task_data[0])
}

/// Convert binary string to a number
fn binary_to_10(binary_num: &String) -> isize {
    let converted: isize = isize::from_str_radix(&binary_num.as_str(), 2).unwrap();
    converted
}

fn read_file(file_path: &str) -> Result<String, Box<dyn Error>> {
    info!("Reading the input file.");
    let contents = fs::read_to_string(file_path)?;
    debug!("File contents: {}", &contents);
    info!("End of Reading the input file.");

    Ok(contents)
}
