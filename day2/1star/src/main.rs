use flexi_logger::Logger;
use log::{debug, info};
use std::env;
use std::error::Error;
use std::fs;

fn main() {
    Logger::try_with_str("info").unwrap().start().unwrap();

    let cl_args: Vec<String> = env::args().collect();

    let task_data: Vec<String> = read_file(&cl_args[2])
        .unwrap()
        .lines()
        .map(|line| line.trim().to_string())
        .collect();

    debug!("Accepted input data.. len={}", &task_data.len());

    let mut x: i32 = 0;
    let mut y: i32 = 0;

    for row in &task_data {
        let (x_row, y_row) = row.split_once(" ").unwrap();
        let y_row: i32 = y_row.parse().unwrap();

        match x_row {
            "forward" => x += y_row,
            "down" => y += y_row,
            "up" => y -= y_row,
            _ => eprintln!("unsupported operation: "),
        };
    }

    info!("final location: {}, {}", x, y);
    info!("Result: {}", x * y);
}

fn read_file(file_path: &str) -> Result<String, Box<dyn Error>> {
    info!("Reading the input file.");
    let contents = fs::read_to_string(file_path)?;
    debug!("File contents: {}", &contents);
    info!("End of Reading the input file.");

    Ok(contents)
}
