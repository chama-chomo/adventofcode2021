use flexi_logger::Logger;
use log::{debug, info, warn};
use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::fs;

fn main() {
    Logger::try_with_str("info").unwrap().start().unwrap();

    // potential Bingo numbers
    let random_number: Vec<i32> = vec![
        17, 58, 52, 49, 72, 33, 55, 73, 27, 69, 88, 80, 9, 7, 59, 98, 63, 42, 84, 37, 87, 28, 97,
        66, 79, 77, 61, 48, 83, 5, 94, 26, 70, 12, 51, 82, 99, 45, 22, 64, 10, 78, 13, 18, 15, 39,
        8, 30, 68, 65, 40, 21, 6, 86, 90, 29, 60, 4, 38, 3, 43, 93, 44, 50, 41, 96, 20, 62, 19, 91,
        23, 36, 47, 92, 76, 31, 67, 11, 0, 56, 95, 85, 35, 16, 2, 14, 75, 53, 1, 57, 81, 46, 71,
        54, 24, 74, 89, 32, 25, 34,
    ];

    let cl_args: Vec<String> = env::args().collect();

    // obtain input data
    let task_data: Vec<String> = read_file(&cl_args[2])
        .unwrap()
        .lines()
        .map(|line| line.trim().to_string())
        .collect();

    // make a boardset
    let boardset = create_boardset(task_data.clone());

    let mut _winning_board: usize = 0;
    let mut _winning_number: i32 = 0;
    let mut _horiz_active_boards: HashMap<usize, HashMap<usize, usize>> = HashMap::new();
    let mut _vert_active_boards: HashMap<usize, HashMap<usize, usize>> = HashMap::new();

    // go through all the boards and store all hits inside active_boards
    for num in &random_number {
        info!("Processing number {}", num);

        for (board_pos, board) in boardset.iter().enumerate() {
            info!("Checking up board: {:?}", &board_pos);

            let horiz_board_key = _horiz_active_boards
                .entry(board_pos)
                .or_insert(HashMap::new());
            let vert_board_key = _vert_active_boards
                .entry(board_pos)
                .or_insert(HashMap::new());

            for elem_pos in 0..5 {
                let column_key = vert_board_key.entry(elem_pos).or_insert(0);
                let line_key = horiz_board_key.entry(elem_pos).or_insert(0);

                // pick up column and line of the board
                let column: Vec<i32> = board.iter().map(|c| c[elem_pos]).collect();
                let line: &Vec<i32> = &board[elem_pos];

                debug!("Checking Line: {:?}", line);
                debug!("Checking Column: {:?}", column);

                for n in line {
                    if &n == &num {
                        *line_key += 1;
                        info!(
                            "{} Horiz Hit! {} in line {:?} in Board {:?}",
                            &line_key, &n, &line, &board
                        );
                    }
                }

                for colnum in column {
                    if &colnum == num {
                        *column_key += 1;
                        info!(
                            "{} Verti Hit! {} in line {:?} in Board {:?}",
                            &column_key, &num, &line, &board
                        );
                    }
                }

                // should we hit all numbers in a row or a column we store the board index and the bingo number
                if (column_key == &5 || line_key == &5) && _winning_number == 0 {
                    debug!("Board in position {} wins!", board_pos);
                    debug!(
                        "Column Key count: {} / Line Key count: {}",
                        &column_key, &line_key
                    );
                    _winning_number = num.clone();
                    _winning_board = board_pos;
                    debug!("Horiz board {:?}", &_horiz_active_boards);
                    debug!("Vert board {:?}", &_vert_active_boards);
                    info!("Winning number .. {:?}", &_winning_number);
                    info!("Board winner.. {:?}", &boardset[_winning_board]);
                    break;
                }
            }

            if &_winning_number > &0 {
                break;
            }
        }
        if &_winning_number > &0 {
            break;
        }
    }

    // concatenate all the lines in winning board
    let joined_lines_of_board: &Vec<i32> = &boardset[_winning_board].concat();
    debug!("Board winner concatenated: {:?}", joined_lines_of_board);

    // sum all unhit nums on the board
    let num_sum = calculate_sum_board(&_winning_number, &random_number, &joined_lines_of_board);

    // sum multiplied by winning number should give the result
    info!("Final num = {}", num_sum * _winning_number)
}

fn calculate_sum_board(stop_num: &i32, nums_not_tobe_counted: &Vec<i32>, board: &Vec<i32>) -> i32 {
    let mut result_num: i32 = 0;

    for num in nums_not_tobe_counted {
        match num {
            numb if !board.contains(num) => {
                debug!("Adding num to sum: {}", &numb);
                result_num += numb
            }
            _ => {
                warn!("Skipping {}, found on board", &num)
            }
        }
        if num == stop_num {
            break;
        }
    }
    info!("Sum board is: {}", &result_num);
    result_num
}

fn create_boardset(data: Vec<String>) -> Vec<Vec<Vec<i32>>> {
    let mut bbs: Vec<Vec<Vec<i32>>> = vec![];
    let mut bb: Vec<Vec<i32>> = vec![];

    for line in data {
        if line.trim().is_empty() {
            debug!("Line is empty: {}, ignoring", line);
            let bb_copy = bb.to_owned();
            bb.clear();
            bbs.push(bb_copy);
            continue;
        } else {
            let line_to_vec: Vec<i32> = line
                .split_whitespace()
                .map(|s| s.parse::<i32>().unwrap())
                .collect();

            if &bb.len() < &5 {
                bb.push(line_to_vec);
            }
        }
    }
    bbs
}

fn read_file(file_path: &str) -> Result<String, Box<dyn Error>> {
    info!("Reading the input file.");
    let contents = fs::read_to_string(file_path)?;
    debug!("File contents: {}", &contents);
    info!("End of Reading the input file.");

    Ok(contents)
}
