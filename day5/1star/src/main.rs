use flexi_logger::Logger;
use log::{debug, info, warn};
use regex::Regex;
use std::env;
use std::error::Error;
use std::fs;

fn main() {
    Logger::try_with_str("debug").unwrap().start().unwrap();

    let cl_args: Vec<String> = env::args().collect();

    // obtain input data
    let task_data: Vec<String> = read_file(&cl_args[2])
        .unwrap()
        .lines()
        .map(|line| line.trim().to_string())
        .collect();

    println!("Input: {:?}", task_data);

    let mut xy_array: Vec<Vec<u32>> = vec![vec![0; 1000]; 1000];

    for line in task_data {
        let (x1, y1, x2, y2) = parse_line(line);

        if x1 == x2 && y1 < y2 {
            for point in y1..y2 + 1 {
                let y_array = &mut xy_array[point];
                y_array[x1] += 1;
            }
        } else if x1 == x2 && y1 > y2 {
            for point in y2..y1 + 1 {
                let y_array = &mut xy_array[point];
                y_array[x1] += 1;
            }
        }

        if y1 == y2 && x1 < x2 {
            let y_array = &mut xy_array[y1];
            for point in x1..x2 + 1 {
                y_array[point] += 1;
            }
        } else if y1 == y2 && x1 > x2 {
            let y_array = &mut xy_array[y1];
            for point in x2..x1 + 1 {
                y_array[point] += 1;
            }
        }
    }

    let xy_array = xy_array.concat();
    let result = xy_array
        .iter()
        .filter(|&&point| point > 1)
        .collect::<Vec<&u32>>();
    info!("result {:?}", result.len())
}

fn parse_line(line: String) -> (usize, usize, usize, usize) {
    debug!("Parsing string: {}", line);
    let re = Regex::new(r"(\d+),(\d+) -> (\d+),(\d+)").unwrap();
    let cap = re.captures(line.as_str()).unwrap();

    (
        cap[1].parse::<usize>().unwrap(),
        cap[2].parse::<usize>().unwrap(),
        cap[3].parse::<usize>().unwrap(),
        cap[4].parse::<usize>().unwrap(),
    )
}

fn read_file(file_path: &str) -> Result<String, Box<dyn Error>> {
    info!("Reading the input file.");
    let contents = fs::read_to_string(file_path)?;
    debug!("File contents: {}", &contents);
    info!("End of Reading the input file.");

    Ok(contents)
}
