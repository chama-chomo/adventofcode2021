use flexi_logger::Logger;
use log::{debug, error, info};
use std::error::Error;
use std::fs;
use std::{char, env};

fn main() {
    Logger::try_with_str("debug").unwrap().start().unwrap();

    let cl_args: Vec<String> = env::args().collect();

    let task_data: Vec<String> = read_file(&cl_args[2])
        .unwrap()
        .lines()
        .map(|line| line.trim().to_string())
        .collect();

    debug!("Accepted input data.. len={}", &task_data.len());

    let mut gamma_rate: Vec<i32> = vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    for row in &task_data {
        let transformed_data: Vec<u32> =
            row.chars().map(|char| char.to_digit(10).unwrap()).collect();

        for (pos, num) in transformed_data.iter().enumerate() {
            match num {
                0 => gamma_rate[pos] = gamma_rate[pos] - 1,
                1 => gamma_rate[pos] = gamma_rate[pos] + 1,
                _ => error!("Not a binary number"),
            }
        }
    }

    let mut gamma_rate_bin: Vec<i32> = vec![];

    for field in gamma_rate {
        gamma_rate_bin.push(binary_from_sign(&field))
    }

    // binary string of gamma rate
    let grs: String = gamma_rate_bin.iter().map(|i| i.to_string()).collect();
    // signed number of gamma rate
    let gamma_rate: isize = binary_to_10(&grs);

    let epsr: String = invert_binary(&grs);
    let epsilon_rate: isize = binary_to_10(&epsr);

    info!(
        "Computed array gamma/epsilon {:?}/{:?}",
        gamma_rate, epsilon_rate
    );
    info!("Power consumption:  {:?}", epsilon_rate * gamma_rate)
}

fn invert_binary(binary: &String) -> String {
    debug!("Got binary: {}", &binary);
    let inverted: String = binary
        .chars()
        .map(|s| match s.to_digit(10).unwrap() {
            0 => "1",
            1 => "0",
            _ => "",
        })
        .collect();

    inverted
}

/// Convert binary string to a number
fn binary_to_10(binary_num: &String) -> isize {
    let converted: isize = isize::from_str_radix(&binary_num.as_str(), 2).unwrap();
    converted
}

/// Return 0 or 1 if input number is negative or positive
fn binary_from_sign(num: &i32) -> i32 {
    if num <= &0 {
        return 0;
    } else {
        return 1;
    }
}

fn read_file(file_path: &str) -> Result<String, Box<dyn Error>> {
    info!("Reading the input file.");
    let contents = fs::read_to_string(file_path)?;
    debug!("File contents: {}", &contents);
    info!("End of Reading the input file.");

    Ok(contents)
}
