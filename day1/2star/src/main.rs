use flexi_logger::Logger;
use log::{debug, info};
use std::env;
use std::error::Error;
use std::fs;

fn main() {
    Logger::try_with_str("info").unwrap().start().unwrap();

    let cl_args: Vec<String> = env::args().collect();

    let radar_data: Vec<String> = read_file(&cl_args[2])
        .unwrap()
        .lines()
        .map(|line| line.trim().to_string())
        .collect();

    debug!("Accepted input data.. len={}", &radar_data.len());

    let mut last_window_sum: u16 = 0;
    let mut counter: u16 = 0;

    for slice in radar_data.windows(3) {
        let previous = slice.first().unwrap().parse::<u16>().unwrap();
        let current = slice[1].parse::<u16>().unwrap();
        let next = slice.last().unwrap().parse::<u16>().unwrap();

        let current_window_sum: u16 = previous + current + next;

        if last_window_sum == 0 {
            last_window_sum = current_window_sum;
            continue;
        };

        info!("Summing up: {} + {} + {}", &previous, &current, &next);

        if current_window_sum > last_window_sum {
            println!(
                "Increased spotted: {} > {}",
                &current_window_sum, &last_window_sum
            );
            counter += 1;
        };

        last_window_sum = current_window_sum;
    }

    println!("Increased level occurrences = {}", counter);
}

fn read_file(file_path: &str) -> Result<String, Box<dyn Error>> {
    info!("Reading the input file.");
    let contents = fs::read_to_string(file_path)?;
    debug!("File contents: {}", &contents);
    info!("End of Reading the input file.");

    Ok(contents)
}
